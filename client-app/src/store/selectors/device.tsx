import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../Store";

export const getDeviceByUuid = createSelector(
  (state: RootState) => state.devices.listOfDevices,
  (_: any, uuid: string | undefined) => uuid,
  (devices, uuid) => {
    if (devices && devices.length > 0 && uuid) {
      const device = devices.filter((d) => d.uuid === uuid);
      if (device && device.length > 0) {
        return { ...device[0], channels: undefined };
      }
    }

    return undefined;
  }
);

export const getDeviceByChannels = createSelector(
  (state: RootState) => state.devices.listOfChannels,
  (_: any, uuid: string | undefined) => uuid,
  (channels, uuid) => {
    if (channels && Object.keys(channels).length > 0 && uuid) {
      const deviceChannels = channels[uuid];
      if (deviceChannels && deviceChannels.length > 0) {
        return deviceChannels;
      }
    }

    return [];
  }
);
