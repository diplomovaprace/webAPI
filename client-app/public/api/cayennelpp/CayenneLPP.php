<?php

include("./cayennelpp/Endian.php");
include("./cayennelpp/types/DigitalInput.php");
include("./cayennelpp/types/DigitalOutput.php");
include("./cayennelpp/types/AnalogInput.php");
include("./cayennelpp/types/AnalogOutput.php");
include("./cayennelpp/types/Luminosity.php");
include("./cayennelpp/types/Presence.php");
include("./cayennelpp/types/Temperature.php");
include("./cayennelpp/types/RelativeHumidity.php");
include("./cayennelpp/types/Accelerometer.php");
include("./cayennelpp/types/BarometricPressure.php");
include("./cayennelpp/types/Gyrometer.php");
include("./cayennelpp/types/GPS.php");

const LPP_HEADER_SIZE = 2;

class CayenneLPPDecoder implements Iterator, Countable
{
    use Endian,
        DigitalInput,
        DigitalOutput,
        AnalogInput,
        AnalogOutput,
        Luminosity,
        Presence,
        Temperature,
        RelativeHumidity,
        Accelerometer,
        BarometricPressure,
        Gyrometer,
        GPS;

    public $data;
    private $index;
    private $type2size;
    private $type2name;

    public function __construct(string $payload)
    {
        $this->type2size = array(
            LPP_ACCELEROMETER         => LPP_ACCELEROMETER_SIZE,
            LPP_ANALOG_INPUT          => LPP_ANALOG_INPUT_SIZE,
            LPP_ANALOG_OUTPUT         => LPP_ANALOG_OUTPUT_SIZE,
            LPP_BAROMETRIC_PRESSURE   => LPP_BAROMETRIC_PRESSURE_SIZE,
            LPP_DIGITAL_INPUT         => LPP_DIGITAL_INPUT_SIZE,
            LPP_DIGITAL_OUTPUT        => LPP_DIGITAL_OUTPUT_SIZE,
            LPP_GPS                   => LPP_GPS_SIZE,
            LPP_GYROMETER             => LPP_GYROMETER_SIZE,
            LPP_LUMINOSITY            => LPP_LUMINOSITY_SIZE,
            LPP_PRESENCE              => LPP_PRESENCE_SIZE,
            LPP_RELATIVE_HUMIDITY     => LPP_RELATIVE_HUMIDITY_SIZE,
            LPP_TEMPERATURE           => LPP_TEMPERATURE_SIZE,
        );

        $this->type2name = array(
            LPP_ACCELEROMETER         => 'accelerometer',
            LPP_ANALOG_INPUT          => 'analogInput',
            LPP_ANALOG_OUTPUT         => 'analogOutput',
            LPP_BAROMETRIC_PRESSURE   => 'pressure',
            LPP_DIGITAL_INPUT         => 'digitalInput',
            LPP_DIGITAL_OUTPUT        => 'digitalOutput',
            LPP_GPS                   => 'gps',
            LPP_GYROMETER             => 'gyrometer',
            LPP_LUMINOSITY            => 'luminosity',
            LPP_PRESENCE              => 'presence',
            LPP_RELATIVE_HUMIDITY     => 'humidity',
            LPP_TEMPERATURE           => 'temperature',
        );

        $this->data = $this->decode($payload);
        $this->index = 0;
    }

    private function decode(string $payload): array
    {
        $out = array();
        $channel = 0;
        $type = 0;

        // Detect empty payload
        if ($payload === '') {
            return $out;
        }

        while (true) {
            if (strlen($payload) < LPP_HEADER_SIZE) {
                throw new Exception("Header is too short");
            }

            $channel = unpack('C', $payload[0])[1];
            $type = unpack('C', $payload[1])[1];
            $size = $this->gettypesize($type) - LPP_HEADER_SIZE;
            $chunck = substr($payload, LPP_HEADER_SIZE, $size);
            if (strlen($chunck) !== $size) {
                throw new Exception('Incomplete data');
            }

            $out[] = array(
                'channel' => $channel,
                'type' => $type,
                'typeName' => $this->type2name[$type],
                'data' => $this->decodeType($type, $chunck)
            );

            $payload = substr($payload, $size + LPP_HEADER_SIZE);
            if (strlen($payload) === 0) {
                break;
            }
        }

        return $out;
    }

    private function decodeType(int $type, string $data): array
    {
        switch ($type) {
            case LPP_ACCELEROMETER:
                return $this->decodeAccelerometer($data);

            case LPP_ANALOG_INPUT:
                return $this->decodeAnalogInput($data);

            case LPP_ANALOG_OUTPUT:
                return $this->decodeAnalogOutput($data);

            case LPP_BAROMETRIC_PRESSURE:
                return $this->decodeBarometricPressure($data);

            case LPP_DIGITAL_INPUT:
                return $this->decodeDigitalInput($data);

            case LPP_DIGITAL_OUTPUT:
                return $this->decodeDigitalOutput($data);

            case LPP_GPS:
                return $this->decodeGPS($data);

            case LPP_GYROMETER:
                return $this->decodeGyrometer($data);

            case LPP_LUMINOSITY:
                return $this->decodeLuminosity($data);

            case LPP_PRESENCE:
                return $this->decodePresence($data);

            case LPP_RELATIVE_HUMIDITY:
                return $this->decodeRelativeHumidity($data);

            case LPP_TEMPERATURE:
                return $this->decodeTemperature($data);

            default:
                return array();
        }
    }

    private function gettypesize(int $type): int
    {
        if ($this->istypesupported($type) === false) {
            throw new Exception('Unknown type');
        }

        return $this->type2size[$type];
    }

    private function istypesupported(int $type): bool
    {
        return array_key_exists($type, $this->type2size);
    }

    public function current()
    {
        return $this->data[$this->index];
    }

    public function key()
    {
        return $this->index;
    }

    public function next()
    {
        $this->index += 1;
    }

    public function rewind()
    {
        $this->index = 0;
    }

    public function valid(): bool
    {
        return isset($this->data[$this->index]);
    }

    public function count(): int
    {
        return count($this->data);
    }
}


class CayenneLPPEncoder
{
    use Endian,
        DigitalInput,
        DigitalOutput,
        AnalogInput,
        AnalogOutput,
        Luminosity,
        Presence,
        Temperature,
        RelativeHumidity,
        Accelerometer,
        BarometricPressure,
        Gyrometer,
        GPS;

    private $buffer;

    public function __construct()
    {
        $this->clearData();
    }

    protected function addData(int $channel, int $type, array $payload)
    {
        $this->buffer[] = $channel;
        $this->buffer[] = $type;
        $this->buffer = array_merge($this->buffer, $payload);
    }

    public function getSize(): int
    {
        return count($this->buffer);
    }

    public function getBuffer(): string
    {
        return pack('C*', ...$this->buffer);
    }

    public function clearData()
    {
        $this->buffer = array();
    }
}
