<?php

class Core
{
	private $isProduction = true;
	private $dbPrefix = '';

	public function __construct($modeProd)
	{
		$this->isProduction = $modeProd;
		require_once('core.config.php');
	}

	function getTableName($tableName)
	{
		return $this->dbPrefix . $tableName;
	}

	function coreDbStart()
	{
		if (isset($this->dbPort) && $this->dbPort != null && $this->dbPort > 0) {
			$this->dbConnection = mysqli_connect($this->dbServer, $this->dbLogin, $this->dbPassw, $this->dbName, $this->dbPort);
		}
		else {
			$this->dbConnection = mysqli_connect($this->dbServer, $this->dbLogin, $this->dbPassw, $this->dbName);
		}
		
		if (mysqli_connect_error()) $this->coreDbError();

		$this->coreDbQuery("SET NAMES 'UTF8'");
	}

	function coreDbStop()
	{
		if ($this->dbConnection) mysqli_close($this->dbConnection);
	}

	function coreDbFetchResult($result)
	{
		return $result->fetch_assoc();
	}

	function coreDbFetchNResultsToArray($result, $n)
	{
		$rows = array();
		$i = 0;
		while ($r = $result->fetch_assoc()) {
			$rows[] = $r;
			$i += 1;
			if ($i == $n) {
				break;
			}
		}
		return $rows;
	}

	function coreDbFetchAllResultsToArray($result)
	{
		$rows = array();
		while ($r = $result->fetch_assoc()) {
			$rows[] = $r;
		}
		return $rows;
	}

	function coreDbFetchResultRow($result)
	{
		return $result->fetch_row();
	}

	function coreDbQuery($query)
	{
		$query_result = mysqli_query($this->dbConnection, $query);
		if (!$this->coreDbError($query)) {
			return $query_result;
		}
	}

	function coreDbError($query = '')
	{
		$errorString = mysqli_error($this->dbConnection);
		if ($errorString) {
			$this->coreError($query . ': ' . $errorString);
		} else {
			return 0;
		}
	}

	function real_escape_string($string)
	{
		return mysqli_real_escape_string($this->dbConnection, $string);
	}

	function coreError($core_error = '')
	{
		if ($core_error) {
			$handle = fopen('./core/error.log', 'a');
			if ($handle) {
				fputs($handle, date('d.m.Y H:i:s', time()) . ' - ' . $core_error . "\n");
				fclose($handle);
			}
			die($core_error);
		}
	}
}
