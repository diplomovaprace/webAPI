import React from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
  Brush,
} from "recharts";

interface DataBase {
  name: string;
}

interface Data extends DataBase {}

interface LineChartProps {
  data: Data[];
}

const colors = [
  { fill: "rgba(29, 233, 182, 0.6)", stroke: "#00bfa5" }
]

function LineChart({ data }: LineChartProps) {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <AreaChart
        className="chart"
        data={data}
        margin={{
          top: 10,
          right: 30,
          left: 0,
          bottom: 0,
        }}
      >
        <CartesianGrid stroke="rgba(255, 255, 255, 0.2)"></CartesianGrid>
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Area type="monotone" dataKey="uv" stroke={colors[0].stroke} fill={colors[0].fill} />
        <Brush dataKey="name" />
      </AreaChart>
    </ResponsiveContainer>
  );
}

export default LineChart;
