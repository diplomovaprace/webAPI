<?php
    require_once('./core/statuscode.php');
    require_once('./tables.php');

    class Devices {
        public function __construct(&$coreObj)
        {
            $this->core = $coreObj;
            $this->statusCode = new StatusCode();
        }

        function listDevices()
        {
            $this->core->coreDbStart();
            $rows = array();
            $rows = $this->core->coreDbFetchAllResultsToArray($this->core->coreDbQuery("SELECT `uuid`, `name`, `ttn_id` as `ttnId`, `last_sync` as `lastSync`, `application_id` as `appId`, `last_period` as `lastPeriod`, `last_period_unit` as `lastPeriodUnit`, `next_period` as `nextPeriod`, `next_period_unit`  as `nextPeriodUnit` FROM ".dbTableDevice." ORDER BY name, lastSync, uuid"));
            $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 200);
            echo json_encode(array( "devices" => $rows ));
            $this->core->coreDBStop();
        }

        function inboundDevices($deviceId, $pageSize, $page) 
        {
            if (!isset($pageSize) | !isset($page))
            {                
                echo json_encode(array("message" => "Invalid query parameters", "type" => "danger"));
                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
                return;
            }

            $query = "";

            if (isset($deviceId))
            {
                $query = "SELECT `device_uuid` as `uuid`, `name` as `deviceName`, `when`, `payload`, `whenin`, `hex` FROM " . dbTableDeviceDataRaw . " RawDataTab LEFT JOIN " . dbTableDevice ." DvcTab ON DvcTab.uuid = RawDataTab.device_uuid WHERE DvcTab.uuid = " . $deviceId . "";
            } 
            else 
            {
                $query = "SELECT `device_uuid` as `uuid`, `name` as `deviceName`, `when`, `payload`, `whenin`, `hex` FROM " . dbTableDeviceDataRaw . " RawDataTab LEFT JOIN " . dbTableDevice ." DvcTab ON DvcTab.uuid = RawDataTab.device_uuid";
            }

            if (isset($_GET["since"]))
            {
                $time_micro_precision = substr($_GET["since"], 0, -4) . 'Z'; // drop the last three digits to move from nanoseconds to microseconds.
                $datetime = new DateTime($time_micro_precision);
                $query += " AND `when` >= STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s')";
            }

            if (isset($_GET["to"])) {
                $time_micro_precision = substr($_GET["to"], 0, -4) . 'Z'; // drop the last three digits to move from nanoseconds to microseconds.
                $datetime = new DateTime($time_micro_precision);
                $query += " AND `when` <= STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s')";
            }

            $query = $query . " ORDER BY `when` DESC LIMIT " . ($page - 1)*$pageSize . "," . $pageSize;

            $this->core->coreDbStart();
            $rows = array();
            
            $dbrows = $this->core->coreDbFetchAllResultsToArray($this->core->coreDbQuery($query));
            
            foreach ($dbrows as $row) {
                try { 
                    $preproc = $row;

                    if (isset($row["payload"]))
                    {
                        $binary = base64_decode($row["payload"]);
                        $decoded = new CayenneLPPDecoder($binary);
                        
                        $preproc["decoded"] = $decoded;
                    }
                    $rows[] = $preproc;
                }
                catch (\Exception $e)
                {
                    continue;
                }
            }
            $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 200);
            echo json_encode(array('data' => $rows), true);
            $this->core->coreDBStop();
        }

        function listChannels($deviceId)
        {
            if (!isset($deviceId))
            {                
                echo json_encode(array("message" => "Invalid query parameters", "type" => "danger"));
                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
                return;
            }

            $this->core->coreDbStart();
            $rows = array();
            $rows = $this->core->coreDbFetchAllResultsToArray($this->core->coreDbQuery("SELECT DISTINCT `channel` as `channelNo`, `datatype` as `dataType` FROM ". dbTableDeviceData ." WHERE device_uuid = " . $deviceId));
            $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 200);
            echo json_encode(array( "channels" => $rows ));
            $this->core->coreDBStop();
        }

        function getDeviceDataByChannelAndDataType($deviceId, $channel, $dataType)
        {
            if (!isset($deviceId) || !isset($channel) || !isset($dataType))
            {                
                echo json_encode(array("message" => "Invalid query parameters", "type" => "danger"));
                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
                return;
            }

            $query = "";
            $this->core->coreDbStart();

            if (isset($_GET["since"]))
            {
                $datetime = new DateTime($_GET["since"]);
                $query = $query . " AND `when` >= STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y 00:00:00')) . "', '%m-%d-%Y %H:%i:%s')";
            }

            if (isset($_GET["to"])) {
                $datetime = new DateTime($_GET["to"]);
                $query = $query . " AND `when` <= STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y 23:59:59')) . "', '%m-%d-%Y %H:%i:%s')";
            }
            
            if (!isset($_GET["since"]) && !isset($_GET["to"])) {
                $query = $query . " AND `when` >= CURDATE()";
            }
            
            $rows = array();
            $dbrows = $this->core->coreDbFetchAllResultsToArray($this->core->coreDbQuery("SELECT `data`, `when` FROM ". dbTableDeviceData ." WHERE device_uuid = " . $deviceId . " AND channel = " . $channel . " AND datatype = " . $dataType . $query . " ORDER BY `when`"));

            if (count($dbrows) == 0 && !isset($_GET["since"]) && !isset($_GET["to"]))
            {
                $dbrows = $this->core->coreDbFetchAllResultsToArray($this->core->coreDbQuery("SELECT `data`, `when` FROM ". dbTableDeviceData ." WHERE device_uuid = " . $deviceId . " AND channel = " . $channel . " AND datatype = " . $dataType . " ORDER BY `when` LIMIT 0, 600"));
            }

            foreach ($dbrows as $row) {
                try { 
                    $preproc = json_decode($row["data"]);
                    $preproc->name = $row["when"];
                    $rows[] = $preproc;
                }
                catch (\Exception $e)
                {
                    continue;
                }
            }

            $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 200);
            echo json_encode(array('data' => $rows), true);
            $this->core->coreDBStop();
        }
    }
