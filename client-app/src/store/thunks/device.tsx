import { createAsyncThunk } from "@reduxjs/toolkit";
import { apiGet, URL_GET_DEVICES, URL_GET_DEVICES_CHANNEL } from "../../hooks";

export const getListOfDevices = createAsyncThunk(
  "devices/listAllDevices",
  async () => {
    return await apiGet(URL_GET_DEVICES);
  }
);

export interface GetListOfChannelsThunkArg { 
  deviceUuid: string
}

export const getListOfChannels = createAsyncThunk(
  "devices/listDevicesChannel",
  async (args: GetListOfChannelsThunkArg) => {
    return await apiGet(URL_GET_DEVICES_CHANNEL({ deviceUuid: args.deviceUuid }));
  }
);
