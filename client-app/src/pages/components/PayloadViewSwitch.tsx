import React, { useCallback, useState } from "react";

interface PayloadViewSwitchProps {
  hex: string;
  payload: string;
  decoded?: any;
  children?: undefined | React.ReactNode | React.ReactNode[];
}

const PayloadViewSwitch: React.FC<PayloadViewSwitchProps> = ({
  hex,
  payload,
  decoded,
  children,
}) => {
  const [mode, setMode] = useState<"payload" | "hex" | "decoded">("payload");

  const getOutputMyMode = useCallback(
    (selectedMode: string, hex: string, payload: string, decoded?: any) => {
      switch (selectedMode) {
        case "hex":
          return hex;

        case "decoded":
          return decoded.data ? JSON.stringify(decoded.data) : "";

        case "payload":
        default:
          return payload;
      }
    },
    []
  );

  const setModePayload = useCallback(() => setMode("payload"), []);
  const setModeHex = useCallback(() => setMode("hex"), []);
  const setModeDecoded = useCallback(() => setMode("decoded"), []);

  return (
    <div className="output">
      {mode === "decoded" ? (
        <textarea
          className="output__text"
          value={getOutputMyMode(mode, hex, payload, decoded)}
        />
      ) : (
        <input
          className="output__text"
          value={getOutputMyMode(mode, hex, payload, decoded)}
        />
      )}
      <div className="output__btn">
        <button
          className={`${mode === "payload" && "active"}`}
          onClick={setModePayload}
        >
          BASE64
        </button>
        <button
          className={`${mode === "hex" && "active"}`}
          onClick={setModeHex}
        >
          HEX
        </button>
        <button
          className={`${mode === "decoded" && "active"}`}
          onClick={setModeDecoded}
        >
          Decoded
        </button>
        {children}
      </div>
    </div>
  );
};

export default PayloadViewSwitch;
