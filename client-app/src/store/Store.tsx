import { configureStore } from '@reduxjs/toolkit';
import DevicesReducer from "./DeviceSlice";

export const store = configureStore({
  reducer: {
    devices: DevicesReducer
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch