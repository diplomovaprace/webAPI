import React, { Fragment, useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "../store";
import { getListOfChannels, getListOfDevices } from "../store/thunks/device";
import { Channel } from "../hooks";
import moment from "moment";
import { getDeviceByUuid } from "../store/selectors/device";
import { NavLink } from "react-router-dom";
import { getNameOfLppType } from "../utils";

interface FilterProps {
  onChannelChange: (channel?: Channel) => void;
  onChangeDevice: (deviceUuid?: string) => void;
}

const Filter: React.FC<FilterProps> = ({ onChannelChange, onChangeDevice }) => {
  const dispatch = useDispatch();
  const [selectedDevice, setSelectedDevice] = useState<string>("");
  const [selectedChannel, setSelectedChannel] = useState<string>("");
  const device = useSelector((state) => getDeviceByUuid(state, selectedDevice));

  const {
    listOfDevices,
    loadingListOfDevices,
    loadingListOfChannels,
    listOfChannels,
  } = useSelector((state) => state.devices);

  const onSelectSensor = useCallback(
    async (event: React.ChangeEvent<HTMLSelectElement>) => {
      if (
        listOfDevices &&
        (listOfDevices?.findIndex(
          (device) => device.uuid === event.target.value
        ) > -1 ||
          event.target.value === "")
      ) {
        setSelectedDevice(event.target.value);
        onChangeDevice(event.target.value);

        if (
          event.target.value &&
          (!listOfChannels || !(event.target.value in listOfChannels))
        ) {
          dispatch(getListOfChannels({ deviceUuid: event.target.value }));
        }
      }
    },
    [dispatch, listOfChannels, listOfDevices, onChangeDevice]
  );

  const onSelectChannel = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      const channelAndDataTypeCSV = event.target.value;
      if (
        channelAndDataTypeCSV.split(",").length === 2 ||
        channelAndDataTypeCSV === ""
      ) {
        const channelAndDataType = channelAndDataTypeCSV.split(",");

        const nextChannel = listOfChannels[selectedDevice]?.find(
          (channel) =>
            channel.channelNo.toString() === channelAndDataType[0].toString() &&
            channel.dataType.toString() === channelAndDataType[1].toString()
        );

        if (nextChannel) {
          setSelectedChannel(channelAndDataTypeCSV);
          onChannelChange(nextChannel);
        }

        if (channelAndDataTypeCSV === "") {
          setSelectedChannel("");
          onChannelChange(undefined);
        }
      }
    },
    [listOfChannels, onChannelChange, selectedDevice]
  );

  useEffect(() => {
    if (!listOfDevices) {
      dispatch(getListOfDevices());
    }
  }, [dispatch, listOfDevices]);

  return (
    <table className="table table-fullpage">
      <tbody>
        <tr>
          <td>Device:</td>
          <td>
            <div className="output">
              <select
                disabled={loadingListOfDevices === "pending"}
                value={selectedDevice}
                className={`${selectedDevice && selectedDevice.length > 0 && "output__text"}`}
                placeholder="Select sensor"
                onChange={onSelectSensor}
              >
                <option></option>
                {loadingListOfDevices !== "pending" && (
                  <Fragment key="loaded">
                    {listOfDevices &&
                      listOfDevices.map((device) => (
                        <option key={device.uuid} value={device.uuid}>
                          {device.name}
                        </option>
                      ))}
                  </Fragment>
                )}
              </select>

              <div className="output__btn">
                {selectedDevice && selectedDevice.length > 0 && (
                  <NavLink to={`/device-settings/${selectedDevice}`}>
                    <button>Change settings</button>
                  </NavLink>
                )}
              </div>
            </div>
          </td>
        </tr>
        {selectedDevice && device && (
          <>
            <tr>
              <td>TTN ID</td>
              <td>{device?.ttnId ?? "-"}</td>
            </tr>
            <tr>
              <td>Application ID</td>
              <td>{device?.appId ?? "-"}</td>
            </tr>
            <tr>
              <td>Sync period</td>
              <td>{device?.nextPeriod && device?.nextPeriodUnit ? `${device?.nextPeriod}${device?.nextPeriodUnit.toString() === "1"? "s":""}${device?.nextPeriodUnit.toString() === "2"? "min":""}${device?.nextPeriodUnit.toString() === "3"? "h":""}`:"<Default>"}</td>
            </tr>
            <tr>
              <td>First sync (Created):</td>
              <td>
                {device?.lastSync
                  ? moment.utc(device?.lastSync).local().format("DD.MM.YYYY, HH:mm:ss")
                  : "-"}
              </td>
            </tr>
          </>
        )}
        {selectedDevice && (
          <tr>
            <td>Channel:</td>
            <td>
              <select
                onChange={onSelectChannel}
                value={selectedChannel}
                placeholder="Select sensor"
              >
                <option></option>
                {loadingListOfChannels !== "pending" && (
                  <Fragment key="loaded">
                    {listOfChannels &&
                      listOfChannels[selectedDevice] &&
                      listOfChannels[selectedDevice].map((channel) => {
                        return (
                          <option
                            key={channel.channelNo}
                            value={[
                              channel.channelNo.toString(),
                              channel.dataType.toString(),
                            ]}
                          >
                            {`Channel ${channel.channelNo} - ${getNameOfLppType(
                              channel.dataType
                            )}`}
                          </option>
                        );
                      })}
                  </Fragment>
                )}
              </select>
            </td>
          </tr>
        )}
      </tbody>
    </table>
  );
};

export default Filter;
