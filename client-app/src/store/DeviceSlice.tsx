import { createSlice } from "@reduxjs/toolkit";
import { Sensor, LoadingStatus, Channel } from "../hooks";
import {
  getListOfChannels,
  GetListOfChannelsThunkArg,
  getListOfDevices,
} from "./thunks/device";

interface IStoreState {
  listOfDevices: Sensor[];
  loadingListOfDevices: LoadingStatus;
  listOfChannels: { [key: string]: Channel[] };
  loadingListOfChannels: LoadingStatus;
  isLoadingChartData: boolean;
}

const initialState = {
  loadingListOfDevices: "idle",
  loadingListOfChannels: "idle"
} as IStoreState;

const storeSlice = createSlice({
  name: "devices",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getListOfDevices.pending, (state) => {
      state.loadingListOfDevices = "pending";
    });
    builder.addCase(getListOfDevices.fulfilled, (state, action) => {
      if (action.payload.data && action.payload.data.devices) {
        state.listOfDevices = action.payload.data.devices;
      } else {
        state.listOfDevices = [];
      }
      state.loadingListOfDevices = "idle";
    });
    builder.addCase(getListOfDevices.rejected, (state) => {
      state.listOfDevices = [];
      state.loadingListOfDevices = "failed";
    });

    builder.addCase(getListOfChannels.pending, (state) => {
      state.loadingListOfChannels = "pending";
    });
    builder.addCase(getListOfChannels.fulfilled, (state, action) => {
      if (action.payload.data && action.payload.data.channels) {
        if (!state.listOfChannels) {
          state.listOfChannels = {};
        }
        
        const thunkArgs: GetListOfChannelsThunkArg = action.meta.arg;
        const deviceIdx = state.listOfDevices?.findIndex(
          (d) => d.uuid === thunkArgs.deviceUuid
        );

        if ((deviceIdx !== undefined && deviceIdx > -1)) {
          state.listOfChannels[thunkArgs.deviceUuid] = action.payload.data.channels;
        }
      }

      state.loadingListOfChannels = "idle";
    });
    builder.addCase(getListOfChannels.rejected, (state) => {
      state.loadingListOfChannels = "failed";
    });
  },
});

export const {  } = storeSlice.actions;
export default storeSlice.reducer;
