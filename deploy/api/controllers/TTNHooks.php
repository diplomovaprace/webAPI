<?php
include("./cayennelpp/CayenneLPP.php");
require_once('./core/statuscode.php');
require_once('./tables.php');
require_once('./headers.php');

const DEFAULT_DOWNLINK_PORT = 1;

class TTNHooks
{
    public function __construct(&$coreObj)
    {
        $this->core = $coreObj;
        $this->statusCode = new StatusCode();
    }

    function uplinkMessage()
    {
        try {
            $requestBody = file_get_contents('php://input');

            if (!isset($requestBody)) {
                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 415);
                echo json_encode(array("message" => "Unsupported Media Type", "type" => "danger"));
                return;
            }

            $jsonRequestBody = json_decode($requestBody);

            if (
                !isset($jsonRequestBody) ||
                !isset($jsonRequestBody->uplink_message->frm_payload) ||
                !isset($jsonRequestBody->end_device_ids->device_id) ||
                !isset($jsonRequestBody->received_at)
            ) {
                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 415);
                echo json_encode(array("message" => "Unsupported Media Type", "type" => "danger"));
                return;
            }

            $headers = getRequestHeaders();
            $binary = base64_decode($jsonRequestBody->uplink_message->frm_payload);
            $decoded = new CayenneLPPDecoder($binary);

            $this->core->coreDbStart();

            if (isset($jsonRequestBody->end_device_ids->device_id) && isset($jsonRequestBody->end_device_ids->application_ids->application_id)) {
                $count = $this->core->coreDbFetchResult($this->core->coreDbQuery("SELECT count(*) as total from " . dbTableDevice . " WHERE ttn_id = '" . $this->core->real_escape_string($jsonRequestBody->end_device_ids->device_id) . "' "
                    . " AND `xttsdomain` " . ((isset($headers["x-tts-domain"])) ? "= '" . $this->core->real_escape_string($headers["x-tts-domain"]) . "'" : "IS NULL")
                    . " AND `xdownlinkpush` " . ((isset($headers["x-downlink-push"])) ? "= '" . $this->core->real_escape_string($headers["x-downlink-push"]) . "'" : "IS NULL")
                    . " AND `xdownlinkapikey` " . ((isset($headers["x-downlink-apikey"])) ? "= '" . $this->core->real_escape_string($headers["x-downlink-apikey"]) . "'" : "IS NULL")
                    . " AND `application_id` = '" . $this->core->real_escape_string($jsonRequestBody->end_device_ids->application_ids->application_id) . "'"));

                $time_micro_precision = substr($jsonRequestBody->received_at, 0, -4) . 'Z'; // drop the last three digits to move from nanoseconds to microseconds.
                $datetime = new DateTime($time_micro_precision);

                if ($count["total"] == 0) {
                    try {
                        $this->core->coreDbQuery("INSERT INTO " . dbTableDevice . "(`name`, `ttn_id`, `last_sync`, `xttsdomain`, `xdownlinkpush`, `xdownlinkapikey`, `application_id`) VALUES ('" . $this->core->real_escape_string(trim($jsonRequestBody->end_device_ids->device_id)) . "','" . $this->core->real_escape_string(trim($jsonRequestBody->end_device_ids->device_id)) . "', STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s'), "
                            . ((isset($headers["x-tts-domain"])) ? "'" . $this->core->real_escape_string($headers["x-tts-domain"]) . "'" : "NULL") . ", "
                            . ((isset($headers["x-downlink-push"])) ? "'" . $this->core->real_escape_string($headers["x-downlink-push"]) . "'" : "NULL") . ", "
                            . ((isset($headers["x-downlink-apikey"])) ? "'" . $this->core->real_escape_string($headers["x-downlink-apikey"]) . "'" : "NULL") . ", "
                            . "'" . $this->core->real_escape_string($jsonRequestBody->end_device_ids->application_ids->application_id) . "');");
                    } catch (exception $e) {
                        echo json_encode(array("message" => "Adding device failed", "type" => "danger"));
                        $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
                        return;
                    }
                }

                $device = $this->core->coreDbFetchResult($this->core->coreDbQuery("SELECT `uuid` from " . dbTableDevice . " WHERE `ttn_id` = '" . $this->core->real_escape_string(trim($jsonRequestBody->end_device_ids->device_id)) . "'". " AND `xttsdomain` = " . ((isset($headers["x-tts-domain"])) ? "'" . $this->core->real_escape_string($headers["x-tts-domain"]) . "'" : "NULL")
                    . " AND `xdownlinkpush` = " . ((isset($headers["x-downlink-push"])) ? "'" . $this->core->real_escape_string($headers["x-downlink-push"]) . "'" : "NULL")
                    . " AND `xdownlinkapikey` = " . ((isset($headers["x-downlink-apikey"])) ? "'" . $this->core->real_escape_string($headers["x-downlink-apikey"]) . "'" : "NULL")
                    . " AND `application_id` = '" . $this->core->real_escape_string($jsonRequestBody->end_device_ids->application_ids->application_id) . "'"));

                if (isset($device["uuid"])) {
                    $raw = array("body" => $requestBody, "header" => getRequestHeaders());
                    try {
                        $countRawData = $this->core->coreDbFetchResult($this->core->coreDbQuery("SELECT count(*) AS total FROM " . dbTableDeviceDataRaw . " WHERE `device_uuid` = " . $device["uuid"] . " AND `when` = STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s');"));
                        if ($countRawData["total"] == 0) {
                            $this->core->coreDbQuery("INSERT INTO " . dbTableDeviceDataRaw . "(`device_uuid`, `when`, `whenin`, `rawdata`, `payload`, `hex`) VALUES (" . $device["uuid"] . ", STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s'), UTC_TIMESTAMP, '" . $this->core->real_escape_string(trim(json_encode($raw))) . "', '" . $this->core->real_escape_string(trim($jsonRequestBody->uplink_message->frm_payload)) . "', '" . bin2hex($binary) . "');");
                        }
                    } catch (exception $e) {
                        $this->core->coreDbQuery("INSERT INTO " . dbTableDeviceDataRaw . "(`device_uuid`, `when`, `whenin`, `rawdata`, `payload`, `hex`) VALUES (" . $device["uuid"] . ", STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s'), UTC_TIMESTAMP, '" . $this->core->real_escape_string(trim(json_encode($raw))) . "', NULL, NULL)';");
                    }

                    try {
                        $this->core->coreDbQuery("START TRANSACTION;");
                        $totalError = 0;

                        foreach ($decoded as $data) {
                            $channel = $data['channel'];
                            $type = $data['type'];
                            $data = $data['data'];

                            if (isset($channel) && isset($type) && isset($data)) {
                                try {
                                    $countData = $this->core->coreDbFetchResult($this->core->coreDbQuery("SELECT count(*) AS total FROM " . dbTableDeviceData . " WHERE `device_uuid` = " . $device["uuid"] . " AND `channel` = " . $channel . " AND `datatype` = " . $type . " AND `when` = STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s');"));
                                    if ($countData["total"] == 0) {
                                        $this->core->coreDbQuery("INSERT INTO " . dbTableDeviceData . "(`device_uuid`, `channel`, `datatype`, `when`, `data`) VALUES (" . $device["uuid"] . ", " . $channel . ", " . $type . ", STR_TO_DATE('" . $this->core->real_escape_string($datetime->format('m-d-Y H:i:s')) . "', '%m-%d-%Y %H:%i:%s'), '" . $this->core->real_escape_string(trim(json_encode($data))) . "');");
                                    } else {
                                        $totalError += 1;
                                    }
                                } catch (exception $e) {
                                    $totalError += 1;
                                }
                            }
                        }

                        $this->core->coreDbQuery("COMMIT");
                    } catch (exception $e) {
                        echo json_encode(array("message" => "Adding data failed", "type" => "danger"));
                        $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
                        return;
                    }
                }

                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 200);
            }

            $this->core->coreDBStop();
        } catch (exception $ex) {
            echo json_encode(array("message" => "Unsupported Media Type", "type" => "danger"));
            $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 415);
            return;
        }
    }

    // TTNv3 API CALLS 
    function sendDownlink()
    {
        try {
            $requestBody = file_get_contents('php://input');

            if (!isset($requestBody) || !isset($_GET['deviceId'])) {
                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 415);
                echo json_encode(array("message" => "Unsupported Media Type", "type" => "danger"));
                return;
            }

            $deviceId = $_GET['deviceId'];
            $jsonRequestBody = json_decode($requestBody);

            if (
                !isset($jsonRequestBody) ||
                !isset($jsonRequestBody->unit) ||
                !isset($jsonRequestBody->value) ||
                $jsonRequestBody->unit > 3 || // max. unit: hours
                $jsonRequestBody->unit < 0 ||                
                ($jsonRequestBody->value < 10 && $jsonRequestBody->unit === 1) || 
                ($jsonRequestBody->value < 0 && $jsonRequestBody->unit > 1) || 
                $jsonRequestBody->value > 9999
            ) {
                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 415);
                echo json_encode(array("message" => "Unsupported Media Type", "type" => "danger"));
                return;
            }

            $encoder = new CayenneLPPEncoder();

            $this->core->coreDbStart();

                $count = $this->core->coreDbFetchResult($this->core->coreDbQuery("SELECT count(*) as total from " . dbTableDevice . " WHERE uuid = " . $this->core->real_escape_string($deviceId) . ";"));

                if ($count["total"] == 0) {
                    echo json_encode(array("message" => "Device does not exist", "type" => "danger"));
                    $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
                    return;
                }

                $device = $this->core->coreDbFetchResult($this->core->coreDbQuery("SELECT `xttsdomain`, `xdownlinkpush`, `xdownlinkapikey`, `application_id` from " . dbTableDevice . " WHERE uuid = " . $this->core->real_escape_string($deviceId) . ";"));

                if (isset($device["xttsdomain"]) && isset($device["xdownlinkpush"]) && isset($device["xdownlinkapikey"]) && isset($device["application_id"])) {
                    try {                        
                        $curl = curl_init($device["xdownlinkpush"]);

                        curl_setopt($curl, CURLOPT_URL, $device["xdownlinkpush"]);
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

                        $postHeaders = array(
                            "Authorization: Bearer " . $device["xdownlinkapikey"],
                            "User-Agent: vsb/lorawan",
                            "Content-Type: application/json"
                        );

                        curl_setopt($curl, CURLOPT_HTTPHEADER, $postHeaders);

                        $message = array(
                            $jsonRequestBody->unit & 0xFF, // type
                            (($jsonRequestBody->value / 1000) % 10) * 16 + (($jsonRequestBody->value / 100) % 10),
                            (($jsonRequestBody->value / 10) % 10) * 16 + ($jsonRequestBody->value % 10)
                        );

                        curl_setopt($curl, CURLOPT_POSTFIELDS, '{"downlinks":[{"frm_payload":"' . base64_encode(pack("C*", ...$message)) . '","f_port": ' . ((isset($jsonRequestBody->port)) ? $jsonRequestBody->port : DEFAULT_DOWNLINK_PORT) . ', "priority":"NORMAL"}]}');

                        echo base64_encode($encoder->getBuffer());
                        //for debug only!
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                        $resp = curl_exec($curl);
                        curl_close($curl);
                        // var_dump($resp);

                        $this->core->coreDbQuery("UPDATE " . dbTableDevice . " set next_period = " . $jsonRequestBody->value . ", next_period_unit = " . $jsonRequestBody->unit . " WHERE uuid = " . $deviceId . ";");
                    } catch (exception $e) {
                        $this->core->coreDbQuery("UPDATE " . dbTableDevice . " set next_period = " . $jsonRequestBody->value . ", next_period_unit = " . $jsonRequestBody->unit . " VALUES uuid = " . $deviceId . ";");

                        echo json_encode(array("message" => "Failed to change period", "type" => "danger"));
                        $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
                        return;
                    }
                }

                $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 200);
            

            $this->core->coreDBStop();
        } catch (exception $ex) {
            echo json_encode(array("message" => "Unsupported Media Type", "type" => "danger"));
            $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 415);
            return;
        }
    }
}
