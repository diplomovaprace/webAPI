<?php 

trait Endian
{
    public function isLittleEndian() : bool
    {
        return unpack('S', "\x01\x00")[1] === 1;
    }

    
    public function swap16(string $input) : string
    {
        return $input[1] . $input[0];
    }

    public function swap24(string $input) : string
    {
        return $input[2] . $input[1] . $input[0];
    }
}
