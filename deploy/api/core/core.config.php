<?php
    $this -> dbServer = 'localhost';
    $this -> dbName = 'lorawandata';
    $this -> dbLogin = 'lorawandata';
    $this -> dbPassw = 'lorawandata123';
    $this -> dbPort = 3306;
    $this -> dbPrefix = '';
    $this -> corePrefix = '/';

    if ($this -> isProduction) {
        $this->dbServer = '';
        $this->dbName = '';
        $this->dbLogin = '';
        $this->dbPassw = '';
	    $this -> dbPort = null; // number (use 0 or null in case you DO NOT want to define port)
    }
