import React, { useCallback, useState } from "react";
import {
  Brush,
  CartesianGrid,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { Spinner } from "../../components";
import { ChannelDataUnit, LoadingStatus } from "../../hooks";

interface LinearReChartProps {
  data?: ChannelDataUnit[] | undefined;
  isLoading: LoadingStatus;
}

function randomDataSeed(range: number): ChannelDataUnit[] {
  if (+range > 0 && !isNaN(+range)) {
    const dataSeed = [];
    for (var i = 0; i < range; i++) {
      dataSeed.push({ name: i, value: Math.random() });
    }

    return dataSeed;
  }

  return [];
}

const LinearReChart: React.FC<LinearReChartProps> = ({ data, isLoading }) => {
  const [dataSeed, setDataSeed] = useState(randomDataSeed(100));

  switch (isLoading) {
    case "pending":
      return (
        <div key="failed" className="char-container">
          <div className="char-container__text--empty">
            <div>
              <span className="mr-1">
                <Spinner />
              </span>{" "}
              Loading data
            </div>
          </div>
          <ResponsiveContainer
            className="char-container__chart--empty"
            key="pending"
            width="100%"
            height="100%"
          >
            <LineChart data={data && data.length > 0 ? data : dataSeed}>
              <CartesianGrid stroke="#aaa" strokeDasharray="5 5" />
              <Line
                type="monotone"
                dataKey="value"
                dot={false}
                stroke="#fff"
                animateNewValues
                animationEasing="ease-in-out"
                animationDuration={300}
              />
              <Brush />
            </LineChart>
          </ResponsiveContainer>
        </div>
      );

    case "failed":
      return (
        <div key="failed" className="char-container">
          <div className="char-container__text--empty">
            <div>Oops, something went wrong</div>
          </div>
          <ResponsiveContainer
            className="char-container__chart--empty"
            width="100%"
            height="100%"
          >
            <LineChart data={dataSeed}>
              <CartesianGrid stroke="#aaa" strokeDasharray="5 5" />
              <Line
                type="monotone"
                dataKey="value"
                stroke="#fff"
                dot={false}
                animateNewValues
                animationEasing="ease-in-out"
                animationDuration={300}
              />
              <Brush />
            </LineChart>
          </ResponsiveContainer>
        </div>
      );

    case "idle":
    case "succeeded":
    default:
      if (data && data.length > 0) {
        return (
          <ResponsiveContainer key="success-data" width="100%" height="100%">
            <LineChart data={data}>
              <XAxis dataKey="name" />
              <YAxis domain={['dataMin', 'dataMax']} interval='preserveStartEnd' />
              <CartesianGrid stroke="#69696f" strokeDasharray="5 5" />
              <Line
                type="monotone"
                dataKey="value"
                dot
                stroke="#1de9b6"
                animateNewValues
                min={Math.min(... data?.map((dt) => dt.value ? +dt.value : 0))}
                animationEasing="ease-in-out"
                animationDuration={300}
              />
              <Tooltip />
              <Brush />
            </LineChart>
          </ResponsiveContainer>
        );
      }

      return (
        <div key="success-no-data" className="char-container">
          <div className="char-container__text--empty"> No data to show</div>
          <ResponsiveContainer
            className="char-container__chart--empty"
            width="100%"
            height="100%"
          >
            <LineChart data={dataSeed}>
              <CartesianGrid stroke="#aaa" strokeDasharray="5 5" />
              <Line
                type="monotone"
                dataKey="value"
                stroke="#fff"
                dot={false}
                animateNewValues
                animationEasing="ease-in-out"
                animationDuration={300}
              />
              <Brush />
            </LineChart>
          </ResponsiveContainer>
        </div>
      );
  }
};

export default LinearReChart;
