import moment from "moment";
import React, { useCallback, useEffect, useState } from "react";
import {
  apiGet,
  ChannelData,
  LoadingStatus,
  URL_GET_CHART_DATA,
} from "../../hooks";
import LinearReChart from "./LinearReChart";

interface DataVisualizationProps {
  channel?: string | number;
  channelDataType?: string | number;
  deviceUuid?: string;
}

const DataVisualization: React.FC<DataVisualizationProps> = ({
  deviceUuid,
  channelDataType,
  channel,
}) => {
  const [chartData, setChartData] = useState<ChannelData | null>();
  const [chartDataIsLoading, setChartDataIsLoading] =
    useState<LoadingStatus>("idle");
  const [isLive, setIsLive] = useState(false);
  const [since, setSince] = useState<string | undefined>(undefined);
  const [upTo, setUpTo] = useState<string | undefined>(undefined);

  const today = moment(new Date()).format("YYYY-MM-DD");

  const getChartData = useCallback(async () => {
    if (
      deviceUuid !== undefined &&
      channel !== undefined &&
      channelDataType !== undefined
    ) {
      setChartDataIsLoading("pending");

      try {
        const response = await apiGet(
          URL_GET_CHART_DATA({
            deviceUuid: deviceUuid ?? "",
            channel: channel ?? "",
            dataType: channelDataType ?? "",
          }),
          {
            query: {
              since: since ?? "",
              to: isLive ? "" : upTo ?? "",
            },
          }
        );

        if (response.status === 200) {
          if (response.data) {
            (response.data as ChannelData).data.forEach((item) =>
              item.name = moment.utc(item.name).local().format("DD.MM.YYYY, HH:mm:ss")
            );
            setChartData(response.data);
          } else {
            setChartData({ data: [] } as ChannelData);
          }
          setChartDataIsLoading("succeeded");
        } else {
          setChartDataIsLoading("failed");
        }
      } catch {
        setChartData(null);
        setChartDataIsLoading("failed");
      }
    }
  }, [channel, channelDataType, deviceUuid, since, upTo, isLive]);

  const getChartDataSilently = useCallback(async () => {
    if (
      deviceUuid !== undefined &&
      channel !== undefined &&
      channelDataType !== undefined &&
      isLive
    ) {
      if (chartDataIsLoading === "failed") {
        setChartDataIsLoading("pending");
      }

      try {
        const response = await apiGet(
          URL_GET_CHART_DATA({
            deviceUuid: deviceUuid ?? "",
            channel: channel ?? "",
            dataType: channelDataType ?? "",
          }),
          {
            query: {
              since: since ?? "",
              to: isLive ? "" : upTo ?? "",
            },
          }
        );

        if (response.status === 200) {
          if (response.data) {
            (response.data as ChannelData).data.forEach((item) =>
              item.name = moment.utc(item.name).local().format("DD.MM.YYYY, HH:mm:ss")
            );
            setChartData(response.data);
          } else {
            setChartData({ data: [] } as ChannelData);
          }
          setChartDataIsLoading("succeeded");
        } else {
          setChartDataIsLoading("failed");
        }
      } catch {
        setChartData(null);
        setChartDataIsLoading("failed");
      }
    }

    if (
      !(
        deviceUuid !== undefined &&
        channel !== undefined &&
        channelDataType !== undefined
      )
    ) {
      setChartData(null);
      setChartDataIsLoading("succeeded");
    }
  }, [
    channel,
    channelDataType,
    chartDataIsLoading,
    deviceUuid,
    since,
    upTo,
    isLive,
  ]);

  const onSetIsLive = useCallback(() => {
    setIsLive((state) => !state);
  }, []);

  const onRefresh = useCallback(() => getChartData(), [getChartData]);

  const onChangeSinceDate = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setSince(
        e.target.value ? moment(e.target.value).format("YYYY-MM-DD") : undefined
      );
    },
    []
  );

  const onChangeUpToDate = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setUpTo(
        e.target.value ? moment(e.target.value).format("YYYY-MM-DD") : undefined
      );
    },
    []
  );

  useEffect(() => {
    if (chartDataIsLoading !== "pending") {
      getChartData();
    }
  }, [getChartData]);

  useEffect(() => {
    const refreshInterval = setInterval(getChartDataSilently, 1000);
    return () => clearInterval(refreshInterval);
  }, [getChartDataSilently]);

  return (
    <div className="plot-card">
      <div className="card-btns">
        <span className={`badge ${isLive ? "live" : "no-live"}`}>LIVE</span>
      </div>
      <div className="card-btns">
        <button className="ml-1" onClick={onSetIsLive}>
          {isLive ? "Stop" : "Play"}
        </button>
        <button className="ml-1" disabled={isLive} onClick={onRefresh}>
          Refresh
        </button>
      </div>
      <div className="row">
        <div className="mr-2">
          <b>Select time:</b>
        </div>
        <input
          onChange={onChangeSinceDate}
          type="date"
          value={since}
          max={upTo ?? today}
        />
        <div className="mx-2">-</div>
        <input
          onChange={onChangeUpToDate}
          type="date"
          disabled={isLive}
          value={upTo}
          min={since}
          max={today}
        />
      </div>
      <div className="plot">
        <LinearReChart data={chartData?.data} isLoading={chartDataIsLoading} />
      </div>
    </div>
  );
};

export default DataVisualization;
