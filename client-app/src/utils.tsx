import { CayenneLppType } from "./hooks";

export function getNameOfLppType(datatype: number) {
    switch (datatype.toString()) {
      case CayenneLppType.LPP_DIGITAL_INPUT:
        return "Digital Input";
      case CayenneLppType.LPP_ACCELEROMETER:
        return "Accelerometer";
      case CayenneLppType.LPP_ANALOG_INPUT:
        return "Analog Input";
      case CayenneLppType.LPP_ANALOG_OUTPUT:
        return "Analog Output";
      case CayenneLppType.LPP_BAROMETRIC_PRESSURE:
        return "Pressure";
      case CayenneLppType.LPP_DIGITAL_OUTPUT:
        return "Digital Output";
      case CayenneLppType.LPP_GPS:
        return "GPS";
      case CayenneLppType.LPP_GYROMETER:
        return "Gyrometer";
      case CayenneLppType.LPP_LUMINOSITY:
        return "Luminosity";
      case CayenneLppType.LPP_PRESENCE:
        return "Presence";
      case CayenneLppType.LPP_RELATIVE_HUMIDITY:
        return "Humidity";
      case CayenneLppType.LPP_TEMPERATURE:
        return "Temperature";
    }
  
    return `${datatype}`;
  }