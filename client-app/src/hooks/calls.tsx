import axios from "axios";

interface ApiParams { 
  query?: { [key:string]:string|number }
  body?: { [key:string]:string|number|object }
}

async function apiGet(url: string, params?: ApiParams) {
  let query = "";

  if (params?.query)
  {
    console.log(params.query);
    Object.keys(params.query).forEach((key: string) => {
      if (params.query && key in params.query && params.query[key].toString().length > 0)
      {
        query += `&${key}=${params.query[key] ?? ""}`;
      }
    });

    if (query.length > 0)
    {
      query = query.replace("&","?");
    }
  }

  const result = await axios.get(url + query, {
    responseType: "json",
  });
  if (result.status >= 200 && result.status <= 204) {
    return { data: result.data, status: result.status };
  }

  return { response: result.statusText, status: result.status };
}

async function apiPost(url: string, params?: ApiParams) {
  let query = "";

  if (params?.query)
  {
    Object.keys(params.query).forEach((key: string) => {
      if (params.query && key in params.query)
      {
        query += `&${key}=${params.query[key] ?? ""}`;
      }
    });

    if (query.length > 0)
    {
      query = query.replace("&","?");
    }
  }

  const result = await axios.post(url + query, params?.body ?? {}, {
    responseType: "json",
  });
  if (result.status >= 200 && result.status <= 204) {
    return { data: result.data, status: result.status };
  }

  return { response: result.statusText, status: result.status };
}

export { apiGet, apiPost };
