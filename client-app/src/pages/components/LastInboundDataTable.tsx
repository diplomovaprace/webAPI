import moment from "moment";
import React, { Fragment, useCallback, useEffect, useState } from "react";
import { Spinner } from "../../components";
import {
  apiGet,
  DataTimestamp,
  LoadingStatus,
  URL_GET_LAST_N_INBOUND_DATA,
} from "../../hooks";
import PayloadViewSwitch from "./PayloadViewSwitch";

interface LastInboundDataTableProps {
  deviceUuid?: string;
  count: number;
}

function randomDataSeed(range: number): number[] {
  if (+range > 0 && !isNaN(+range)) {
    const dataSeed = [];
    for (var i = 0; i < range; i++) {
      dataSeed.push(range);
    }

    return dataSeed;
  }

  return [];
}

const LastInboundDataTable: React.FC<LastInboundDataTableProps> = ({
  count,
  deviceUuid,
}) => {
  const [dataSeed, setDataSeed] = useState(randomDataSeed(count));
  const [tableData, setTableData] = useState<DataTimestamp[] | null>();
  const [tableDataIsLoading, setTableDataIsLoading] =
    useState<LoadingStatus>("idle");
  const [isLive, setIsLive] = useState(false);

  const getData = useCallback(async () => {
    setTableDataIsLoading("pending");

    try {
      const response = await apiGet(
        URL_GET_LAST_N_INBOUND_DATA({
          deviceUuid,
          pageSize: count,
        })
      );

      if (response.data && response.status === 200) {
        const responseData = response.data;
        setTableData(responseData.data ?? []);
        setTableDataIsLoading("succeeded");
      } else {
        setTableDataIsLoading("failed");
      }
    } catch {
      setTableData(null);
      setTableDataIsLoading("failed");
    }
  }, [deviceUuid, count]);
  
  const getDataSilently = useCallback(async () => {
    if (
      isLive
    ) {
      if (tableDataIsLoading === "failed") {
        setTableDataIsLoading("pending");
      }

      try {
        const response = await apiGet(
          URL_GET_LAST_N_INBOUND_DATA({
            deviceUuid,
            pageSize: count,
          })
        );
  
        if (response.data && response.status === 200) {
          const responseData = response.data;
          setTableData(responseData.data ?? []);
          setTableDataIsLoading("succeeded");
        } else {
          setTableDataIsLoading("failed");
        }
      } catch {
        setTableData(null);
        setTableDataIsLoading("failed");
      }
    }
  }, [count, deviceUuid, isLive, tableDataIsLoading]);

  const onSetIsLive = useCallback(() => setIsLive((state) => !state), []);  
  const onRefresh = useCallback(() => getData(), [getData]);

  useEffect(() => {
    if (tableDataIsLoading !== "pending") {
      getData();
    }
  }, [getData]);
  
  useEffect(() => {
    const refreshInterval = setInterval(getDataSilently, 1000);
    return () => clearInterval(refreshInterval);
  }, [getDataSilently]);

  let table = null;
  let note = null;
  let tableClassName = "";

  switch (tableDataIsLoading) {
    case "failed":
      tableClassName = "table--error";
      note = (
        <div className="table-wrapper__text table-wrapper__text--error">
          <div>Oops, something went wrong</div>
        </div>
      );
      table = (
        <Fragment key="failed">
          {dataSeed.map((_, idx) => (
            <tr className="transparent" key={idx}>
              <td>-</td>
              <td>-</td>
              <td>-</td>
              <td>-</td>
            </tr>
          ))}
        </Fragment>
      );
      break;

    case "pending":
      tableClassName = "table--empty";
      note = (
        <div className="table-wrapper__text table-wrapper__text--empty">
          <div>
            <span className="mr-1">
              <Spinner />
            </span>{" "}
            Loading data
          </div>
        </div>
      );

      if (tableData) {
        table = (
          <Fragment key="has-data">
            {tableData.map((row, idx) => (
              <tr key={idx}>
                <td>{row.whenin}</td>
                <td>{row.when}</td>
                <td>{row.deviceName}</td>
                <td className="td-max">
                  <PayloadViewSwitch
                    decoded={row.decoded}
                    payload={row.payload}
                    hex={row.hex}
                  />
                </td>
              </tr>
            ))}
            {dataSeed.map((_, idx) =>
              idx > tableData.length ? (
                <tr className="transparent" key={idx}>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                </tr>
              ) : (
                <Fragment key={idx} />
              )
            )}
          </Fragment>
        );
      } else {
        table = (
          <Fragment key="no-data">
            {dataSeed.map((_, idx) => (
              <tr className="transparent" key={idx}>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
              </tr>
            ))}
          </Fragment>
        );
      }

      break;

    case "idle":
    case "succeeded":
    default:
      if (tableData) {
        table = (
          <Fragment key="has-data">
            {tableData.map((row, idx) => (
              <tr key={idx}>                
                <td>{moment.utc(row.whenin).local().format('DD.MM.YYYY, HH:mm:ss')}</td>
                <td>{moment.utc(row.when).local().format('DD.MM.YYYY, HH:mm:ss')}</td>
                <td>{row.deviceName}</td>
                <td className="td-max">
                  <PayloadViewSwitch
                    decoded={row.decoded}
                    payload={row.payload}
                    hex={row.hex}
                  />
                </td>
              </tr>
            ))}
            {dataSeed.map((_, idx) =>
              idx > tableData.length ? (
                <tr className="transparent" key={idx}>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                </tr>
              ) : (
                <Fragment key={idx} />
              )
            )}
          </Fragment>
        );
      } else {
        tableClassName = "table--empty";
        note = (
          <div className="table-wrapper__text table-wrapper__text--empty">
            <div>No data to show</div>
          </div>
        );
        table = (
          <Fragment key="no-data">
            {dataSeed.map((_, idx) => (
              <tr className="transparent" key={idx}>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
              </tr>
            ))}
          </Fragment>
        );
      }
      break;
  }

  return (
    <div className="plot-card">
      <div className="card-btns">
        <span className={`badge ${isLive ? "live" : "no-live"}`}>LIVE</span>
      </div>
      <div className="card-btns">
        <button className="ml-1" onClick={onSetIsLive}>
          {isLive ? "Stop" : "Play"}
        </button>
        <button className="ml-1" onClick={onRefresh} disabled={isLive}>
          Refresh
        </button>
      </div>
      <div className="table-wrapper">
        {note}
        <table className={`table table-fullpage ${tableClassName}`}>
          <thead>
            <tr>
              <th>Sent by device</th>
              <th>Received from TTS</th>
              <th>Device</th>
              <th>Payload</th>
            </tr>
          </thead>
          <tbody>{table}</tbody>
        </table>
      </div>
    </div>
  );
};

export default LastInboundDataTable;
