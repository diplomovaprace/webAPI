export interface Dataset<T> {
  data: T[]
}

export interface DataTimestamp {
  uuid: string,
  deviceName: string,
  when: number;
  whenin: number;
  
  payload: string;
  hex: string;
  decoded: any;
}

export interface Channel {
  dataType: number;
  channelNo: number;
  period?: number
}

export interface Sensor {
  uuid: string;
  name: string;
  ttnId: string;
  lastSync: string;
  channels?: Channel[];
  appId?: string;
  lastPeriod?: number;
  lastPeriodUnit?: number;
  nextPeriod?: number;
  nextPeriodUnit?: number;
}

export interface ChannelDataUnit {
  value: string | number;
  name: string | number;
}

export interface ChannelData {
  data: ChannelDataUnit[];
}

export enum CayenneLppType {
  LPP_ACCELEROMETER = "113",
  LPP_ANALOG_INPUT = "2",
  LPP_ANALOG_OUTPUT = "3",
  LPP_BAROMETRIC_PRESSURE = "115",
  LPP_DIGITAL_INPUT = "0",
  LPP_DIGITAL_OUTPUT = "1",
  LPP_GPS = "136",
  LPP_GYROMETER = "134",
  LPP_LUMINOSITY = "101",
  LPP_PRESENCE = "102",
  LPP_RELATIVE_HUMIDITY = "104",
  LPP_TEMPERATURE = "103",
}

export const CayenneLppKnownTypes: { [key: number]: CayenneLppType } = {
  113: CayenneLppType.LPP_ACCELEROMETER,
  2: CayenneLppType.LPP_ANALOG_INPUT,
  3: CayenneLppType.LPP_ANALOG_OUTPUT,
  115: CayenneLppType.LPP_BAROMETRIC_PRESSURE,
  0: CayenneLppType.LPP_DIGITAL_INPUT,
  1: CayenneLppType.LPP_DIGITAL_OUTPUT,
  136: CayenneLppType.LPP_GPS,
  134: CayenneLppType.LPP_GYROMETER,
  101: CayenneLppType.LPP_LUMINOSITY,
  102: CayenneLppType.LPP_PRESENCE,
  104: CayenneLppType.LPP_RELATIVE_HUMIDITY,
  103: CayenneLppType.LPP_TEMPERATURE,
};

export type LoadingStatus = "idle" | "pending" | "succeeded" | "failed";
