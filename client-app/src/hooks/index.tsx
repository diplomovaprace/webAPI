const API_URL = process.env.REACT_APP_API_PROXY;
export const URL_GET_DEVICES = `${API_URL ?? ''}/api/devices`;

interface UrlGetDevicesChannelProps {
    deviceUuid: string
}
export const URL_GET_DEVICES_CHANNEL = ({ deviceUuid }: UrlGetDevicesChannelProps) => `${API_URL ?? ''}/api/device/${deviceUuid}/channels`;

interface UrlGetChartDataProps {
    deviceUuid: string;
    channel: string | number;
    dataType: string | number;
}

interface UrlGetLastInboundDataProps {
    deviceUuid?: string;
    pageSize: number;
    page?: number;
}

interface UrlPostRequestPeriodChangeProps {    
    deviceUuid: string;
}

export const URL_GET_CHART_DATA = ({ deviceUuid, channel, dataType }: UrlGetChartDataProps) => `${API_URL ?? ''}/api/device/${deviceUuid}/channel/${channel}/${dataType}/data`;
export const URL_GET_LAST_N_INBOUND_DATA = ({ deviceUuid, pageSize, page }: UrlGetLastInboundDataProps) => {
    if (deviceUuid)
    {
        return `${API_URL ?? ''}/api/device/${deviceUuid}/inbound/${pageSize}/${page ?? 1}`
    }
    
    return `${API_URL ?? ''}/api/devices/inbound/${pageSize}/${page ?? 1}`
};
export const URL_POST_REQUEST_PERIOD_CHANGE = ({ deviceUuid }: UrlPostRequestPeriodChangeProps) => `${API_URL ?? ''}/api/ttn/downlink/${deviceUuid}`;

export * from "./models";
export { apiGet } from "./calls";