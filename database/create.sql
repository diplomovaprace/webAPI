-- MYSQL 8.0 Script
-- create database
CREATE DATABASE `lorawandata`; /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

-- create tables
CREATE TABLE `device` (
  `uuid` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `ttn_id` varchar(80) NOT NULL,
  `last_sync` datetime DEFAULT NULL,
  `xttsdomain` varchar(500) DEFAULT NULL,
  `xdownlinkpush` text,
  `xdownlinkapikey` varchar(300) DEFAULT NULL,
  `application_id` varchar(45) NOT NULL,
  `last_period` int DEFAULT NULL,
  `last_period_unit` int DEFAULT NULL,
  `next_period` int DEFAULT NULL,
  `next_period_unit` int DEFAULT NULL,
  PRIMARY KEY (`uuid`)
);

CREATE TABLE `devicedata` (
  `device_uuid` bigint NOT NULL,
  `channel` int NOT NULL,
  `datatype` int NOT NULL,
  `when` datetime NOT NULL,
  `data` longtext NOT NULL,
  PRIMARY KEY (`device_uuid`,`channel`,`datatype`,`when`)
);

CREATE TABLE `devicedataraw` (
  `device_uuid` bigint NOT NULL,
  `when` datetime NOT NULL,
  `whenin` datetime NOT NULL,
  `rawdata` longtext NOT NULL,
  `payload` longtext NOT NULL,
  `hex` longtext,
  PRIMARY KEY (`device_uuid`,`when`,`whenin`)
);
