import React, { useCallback, useState } from "react";
import { Channel } from "../hooks";
import LastInboundDataTable from "./components/LastInboundDataTable";
import Filter from "./Filter";
import DataVisualization from "./visualization/DataVisualization";

const Core: React.FC = () => {
  const [deviceUuid, setDeviceUuid] = useState<string>();
  const [channel, setChannel] = useState<string>();
  const [channelDataType, setChannelDataType] = useState<string>();

  const onChangeDevice = useCallback((uuid: string | undefined) => {
    setDeviceUuid(uuid);
    setChannel(undefined);
    setChannelDataType(undefined);
  }, []);

  const onChannelChange = useCallback((channel?: Channel) => {
    if (channel) {
      setChannel(channel?.channelNo.toString());
      setChannelDataType(channel?.dataType.toString());
    } else {
      setChannel(undefined);
      setChannelDataType(undefined);
    }
  }, []);

  return (
    <div className="container">
      <div className="col-12 pb-0">
        <h1>Dashboard</h1>
      </div>
      <div className="col-12">
        <div className="card mxy-0">
          <h2 className="card-title">Last received data</h2>
          <LastInboundDataTable count={10} deviceUuid={deviceUuid} />
        </div>
      </div>
      <div className="col-6 mb-2">
        <div className="card">
          <h2 className="card-title">Data visualization</h2>
          <DataVisualization
            deviceUuid={deviceUuid}
            channel={channel}
            channelDataType={channelDataType}
          />
        </div>
      </div>
      <div className="col-6 mb-2">
        <div className="card">
          <h2 className="card-title">General informations</h2>
          <Filter
            onChannelChange={onChannelChange}
            onChangeDevice={onChangeDevice}
          />
        </div>
      </div>
    </div>
  );
};

export default Core;
