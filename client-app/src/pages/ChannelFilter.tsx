import React, { useCallback, useState } from "react";
import { CayenneLppType, Channel } from "../hooks";
import { useSelector } from "../store";
import { getDeviceByChannels } from "../store/selectors/device";

interface ChannelFilterProps {
  deviceUuid?: string;
  onChannelChange: (channel?: Channel) => void;
}

function getNameOfLppType(datatype: number) {
  switch (datatype.toString()) {
    case CayenneLppType.LPP_DIGITAL_INPUT:
      return "Digital Input";
    case CayenneLppType.LPP_ACCELEROMETER:
      return "Accelerometer";
    case CayenneLppType.LPP_ANALOG_INPUT:
      return "Analog Input";
    case CayenneLppType.LPP_ANALOG_OUTPUT:
      return "Analog Output";
    case CayenneLppType.LPP_BAROMETRIC_PRESSURE:
      return "Pressure";
    case CayenneLppType.LPP_DIGITAL_OUTPUT:
      return "Digital Output";
    case CayenneLppType.LPP_GPS:
      return "GPS";
    case CayenneLppType.LPP_GYROMETER:
      return "Gyrometer";
    case CayenneLppType.LPP_LUMINOSITY:
      return "Luminosity";
    case CayenneLppType.LPP_PRESENCE:
      return "Presence";
    case CayenneLppType.LPP_RELATIVE_HUMIDITY:
      return "Humidity";
    case CayenneLppType.LPP_TEMPERATURE:
      return "Temperature";
  }

  return `${datatype}`;
}

const ChannelFilter: React.FC<ChannelFilterProps> = ({
  deviceUuid,
  onChannelChange,
}) => {
  const [selectedChannel, setSelectedChannel] = useState<Channel | undefined>();
  const { loadingListOfChannels } = useSelector((state) => state.devices);
  const channels = useSelector((state) =>
    getDeviceByChannels(state, deviceUuid)
  );

  const onSelectChannel = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      const nextChannel = channels?.find(
        (channel) =>
          channel.channelNo.toString() === event.target.value.toString()
      );

      setSelectedChannel(nextChannel);
      onChannelChange(nextChannel);
    },
    [channels, onChannelChange]
  );

  if (loadingListOfChannels === "pending") {
    return <option key="Loading">Loading ...</option>;
  }

  return (
    <tr>
      <td>Channel:</td>
      <td>
        <select
          onChange={onSelectChannel}
          value={selectedChannel?.channelNo}
          placeholder="Select sensor"
        >
          <option></option>

          {channels &&
            channels.map((channel) => {
              return (
                <option
                  key={channel.channelNo}
                  value={[
                    channel.channelNo.toString(),
                    channel.dataType.toString(),
                  ]}
                >
                  {`Channel ${channel.channelNo} - ${getNameOfLppType(
                    channel.dataType
                  )}`}
                </option>
              );
            })}
        </select>
      </td>
    </tr>
  );
};

export default ChannelFilter;
