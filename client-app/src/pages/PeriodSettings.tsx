import React, { useCallback, useState, useEffect } from "react";
import { Navigate, useParams } from "react-router";
import { LoadingStatus, URL_POST_REQUEST_PERIOD_CHANGE } from "../hooks";
import { apiPost } from "../hooks/calls";
import { useDispatch, useSelector } from "../store";
import { getDeviceByUuid } from "../store/selectors/device";
import { getListOfChannels, getListOfDevices } from "../store/thunks/device";

const PeriodSettings: React.FC = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const [selectedUnit, setSelectedUnit] = useState<number>(1);
  const [period, setPeriod] = useState<number>(10);
  const [changingStatus, setChangingStatus] = useState<LoadingStatus>("idle");
  const { deviceUuid } = useParams();

  const device = useSelector((state) => getDeviceByUuid(state, deviceUuid));

  const {
    listOfDevices,
    loadingListOfDevices,
    loadingListOfChannels,
    listOfChannels,
  } = useSelector((state) => state.devices);

  const onChangePeriod = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const amount = event.target.value;

      if (amount && +amount > 0 && Number.isInteger(+amount)) {
        setPeriod(+amount);
      }
    },
    []
  );

  const onSelectUnit = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      const unit = event.target.value;
      if (unit && +unit > 0 && +unit < 4 && Number.isInteger(+unit)) {
        setSelectedUnit(+unit);
      }
    },
    []
  );

  useEffect(() => {
    if (!listOfDevices) {
      dispatch(getListOfDevices())
        .then(() => {
          if (
            deviceUuid &&
            (!listOfChannels || !(deviceUuid in listOfChannels))
          ) {
            dispatch(getListOfChannels({ deviceUuid: deviceUuid })).finally(
              () => {
                setIsLoading(false);
              }
            );
          } else {
            setIsLoading(false);
          }
        })
        .finally(() => {
          setIsLoading(false);
        });
    } else {
      if (deviceUuid && (!listOfChannels || !(deviceUuid in listOfChannels))) {
        dispatch(getListOfChannels({ deviceUuid: deviceUuid })).finally(() => {
          setIsLoading(false);
        });
      } else {
        setIsLoading(false);
      }
    }
  }, []);

  const onSubmitChanges = useCallback(() => {
    setChangingStatus("pending");
    apiPost(URL_POST_REQUEST_PERIOD_CHANGE({ deviceUuid: deviceUuid ?? "" }), {
      body: {
        unit: selectedUnit,
        value: period,
      },
    })
      .then(() => {
        setChangingStatus("succeeded");
        dispatch(getListOfDevices());
      })
      .catch(() => setChangingStatus("failed"));
  }, [deviceUuid, dispatch, period, selectedUnit]);

  if (
    isLoading ||
    loadingListOfDevices === "pending" ||
    loadingListOfChannels === "pending"
  ) {
    return <>Loading ...</>;
  }

  if (!device) {
    return <Navigate to="/" />;
  }

  return (
    <div className="container">
      <div className="col-12 pb-0">
        <h1>Device custom settings</h1>
        <h4 className="mxy-0 text--lighter text--thin">{device.name}</h4>
      </div>
      <div className="col-6 mb-2">
        <div className="card card--max">
          <h2 className="card-title">Period settings</h2>
          <table className="table table-fullpage">
            <tbody>
              <tr>
                <td>Period:</td>
                <td>
                  <div className="output">
                    <input
                      type="number"
                      min={selectedUnit === 1 ? 10 : 1}
                      max={9999}
                      value={period}
                      onChange={onChangePeriod}
                    />
                    <select
                      className="ml-1"
                      value={selectedUnit}
                      onChange={onSelectUnit}
                    >
                      <option value={1}>seconds</option>
                      <option value={2}>minutes</option>
                      <option value={3}>hours</option>
                    </select>
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <button
                    disabled={
                      (selectedUnit === 1 && period < 10) ||
                      period < 1 ||
                      period > 9999 ||
                      changingStatus === "pending"
                    }
                    onClick={onSubmitChanges}
                  >
                    {changingStatus === "pending"
                      ? "Submitting changes ..."
                      : "Submit change"}
                  </button>

                  {changingStatus === "succeeded" && (
                    <span className={`ml-1 text-success`}>
                      Change was successfully requested
                    </span>
                  )}
                  {changingStatus === "failed" && (
                    <span className={`ml-1 text-danger`}>
                      Change request failed
                    </span>
                  )}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};

export default PeriodSettings;
