<?php
require_once('./cors.php');

session_start();
cors();
require_once('./core/core.class.php');
require_once('./core/statuscode.php');
require_once("./controllers/Device.php");
require_once("./controllers/Devices.php");
require_once("./controllers/TTNHooks.php");

if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header("HTTP/1.1 200 OK");
    return;
}

$core = new Core(true); // production mode - true, developement mode - false
$statusCode = new StatusCode();

$controller = "";
$meth = "";

if (isset($_GET["controller"]))
    $controller = $_GET["controller"];

/* URL mapping */
try {
    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':
            switch ($controller) {
                case "devices":
                    if (isset($_GET["meth"]))
                        $meth = $_GET["meth"];

                    $devicesController = new Devices($core);

                    switch ($meth) {
                        case "list":
                            $devicesController->listDevices();
                            return;
                    }
                    break;

                case "channels":
                    if (isset($_GET["meth"]))
                        $meth = $_GET["meth"];

                    $devicesController = new Devices($core);

                    switch ($meth) {
                        case "list":
                            $devicesController->listChannels($_GET["deviceId"]);
                            return;
                    }
                    break;

                case "data":
                    if (isset($_GET["meth"]))
                        $meth = $_GET["meth"];

                    $devicesController = new Devices($core);

                    switch ($meth) {
                        case "list":
                            $devicesController->getDeviceDataByChannelAndDataType($_GET["deviceId"], $_GET["channel"], $_GET["dataType"]);
                            return;

                        case "inbound":
                            $devicesController->inboundDevices($_GET["deviceId"], $_GET["pageSize"], $_GET["page"]);
                            return;

                        case "globalInbound":
                            $devicesController->inboundDevices(null, $_GET["pageSize"], $_GET["page"]);
                            return;
                    }

                    break;
            }
            break;

        case 'POST':
            switch ($controller) {
                case "ttn":
                    if (isset($_GET["meth"]))
                        $meth = $_GET["meth"];

                    $ttnHooksController = new TTNHooks($core);

                    switch ($meth) {
                        case "uplink":
                            $ttnHooksController->uplinkMessage();
                            return;

                        case "downlink":
                            $ttnHooksController->sendDownlink();
                            return;
                    }
                    break;
            }
            break;
    }

    $statusCode->setHttpHeaders('application/json;charset=UTF-8', 404);
} catch (exception $e) {
    $statusCode->setHttpHeaders('application/json;charset=UTF-8', 400);
    echo json_encode(array("message" => "Err: Unexpected error", "type" => "danger"));
}
