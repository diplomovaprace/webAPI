<?php
    require_once('./core/statuscode.php');
    require_once('./tables.php');

    class Device {
        public function __construct(&$coreObj)
        {
            $this->core = $coreObj;
            $this->statusCode = new StatusCode();
        }

        function listActive($isCheckIn, $isCheckOut)
        {
            $this->core->coreDbStart();
            $rows = array();
            $rows = $this->core->coreDbFetchAllResultsToArray($this->core->coreDbQuery("SELECT id, name, icon FROM ".dbTableDevice." WHERE is_active = true AND is_startpoint = ".$isCheckIn." AND is_endpoint = ".$isCheckOut." ORDER BY case when position is null then 1 else 0 end, position, name, id"));
            $this->statusCode->setHttpHeaders('application/json;charset=UTF-8', 200);
            echo json_encode(array( "items" => $rows ));
            $this->core->coreDBStop();
        }
    }
?>