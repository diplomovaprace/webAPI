import React, { lazy, Suspense } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./store";
import "./App.scss";

const Core = lazy(() => import("./pages/Core"));
const PeriodSettings = lazy(() => import("./pages/PeriodSettings"));

const App:React.FC = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Suspense fallback={<>Loading...</>}>
          <Routes>
            <Route path="/device-settings/:deviceUuid" element={<PeriodSettings />} />
            <Route path="/" element={<Core />} />
            <Route path="/*" element={<Navigate to="/" />} />
          </Routes>
        </Suspense>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
